<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function index()
    {
    	return view('register');
    }

    public function save(Request $request)
    {
    	//dd($request->all()); //cek request
    	/*
    	//cara compact data #1 
    	$firstname = strtoupper($request->firstname);
    	$lastname = strtoupper($request->lastname);
    	
    	return view('welcome', compact('firstname','lastname'));
		*/

    	//cata compact data #2
    	$data['firstname'] = strtoupper($request->firstname);
    	$data['lastname'] = strtoupper($request->lastname);

    	return view('welcome', compact('data'));
    }
}
